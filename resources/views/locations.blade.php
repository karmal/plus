@extends('layouts.app')

@section('content')
<div class="container mt-5" id="app">
    <get-nearest-location-component :restaurants="{{$restaurants}}"></get-nearest-location-component>
    <set-new-location-component></set-new-location-component>
</div>



<h1 class="text-center my-2">Locations from cvs file</h1>
<table style="width: 100%;" class="table">
    <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">postcode</th>
            <th scope="col">open monday</th>
            <th scope="col">close monday</th>
            <th scope="col">open tue</th>
            <th scope="col">close tue</th>
            <th scope="col">open we</th>
            <th scope="col">close we</th>
            <th scope="col">open thu</th>
            <th scope="col">close thu</th>
            <th scope="col">open fri</th>
            <th scope="col">close fri</th>
            <th scope="col">open sat</th>
            <th scope="col">close sat</th>
            <th scope="col">open sun</th>
            <th scope="col">close sun</th>
        </tr>
    </thead>

    <tbody>
    <?php
    
    foreach($restaurants as $restaurant){
        // dd($restaurant);
    ?>
        <tr>
            <th scope="row"><?php echo $restaurant['id']; ?></td>
            <td><?php echo $restaurant['postcode']; ?></td>
            <td><?php echo $restaurant['open_Monday']; ?></td>
            <td><?php echo $restaurant['closed_Monday']; ?></td>
            <td><?php echo $restaurant['open_Tuesday']; ?></td>
            <td><?php echo $restaurant['closed_Tuesday']; ?></td>
            <td><?php echo $restaurant['open_Wednesday']; ?></td>
            <td><?php echo $restaurant['closed_Wednesday']; ?></td>
            <td><?php echo $restaurant['open_Thursday']; ?></td>
            <td><?php echo $restaurant['closed_Thursday']; ?></td>
            <td><?php echo $restaurant['open_Friday']; ?></td>
            <td><?php echo $restaurant['closed_Friday']; ?></td>
            <td><?php echo $restaurant['open_Saturday']; ?></td>
            <td><?php echo $restaurant['closed_Saturday']; ?></td>
            <td><?php echo $restaurant['open_Sunday']; ?></td>
            <td><?php echo $restaurant['closed_Sunday']; ?></td>
        </tr>
        <?php
    }
    ?>
</tbody>


    </table>


    @endsection