@extends('layouts.app')

@section('content')

<form action="/count-cashback" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="ristretto">Ristretto ammount</label>
        <input type="number" name="ristretto" class="form-control" id="ristretto" placeholder="Enter amount">
      </div>
      <div class="form-group">
        <label for="espresso">Espresso ammount</label>
        <input type="number" name="espresso" class="form-control" id="espresso" placeholder="Enter amount">
      </div>
      <div class="form-group">
        <label for="lungo">Lungo ammount</label>
        <input type="number" name="lungo" class="form-control" id="lungo" placeholder="Enter amount">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>

    </form>


@endsection