<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CashbackController;
use App\Http\Controllers\RestaurantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::view('/', 'welcome');
Route::get('/locations', [RestaurantController::class, 'index']);
Route::view('/cashback', 'count-cashback');
Route::post('/count-cashback', [CashbackController::class, 'calculateCashback']);
Route::post('/restaurant-add', [RestaurantController::class, 'store']);