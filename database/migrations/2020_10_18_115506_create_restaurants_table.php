<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->id();
            $table->string('postcode');
            $table->string('open_Monday')->nullable();
            $table->string('open_Tuesday')->nullable();
            $table->string('open_Wednesday')->nullable();
            $table->string('open_Thursday')->nullable();
            $table->string('open_Friday')->nullable();
            $table->string('open_Saturday')->nullable();
            $table->string('open_Sunday')->nullable();
            $table->string('closed_Monday')->nullable();
            $table->string('closed_Tuesday')->nullable();
            $table->string('closed_Wednesday')->nullable();
            $table->string('closed_Thursday')->nullable();
            $table->string('closed_Friday')->nullable();
            $table->string('closed_Saturday')->nullable();
            $table->string('closed_Sunday')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
