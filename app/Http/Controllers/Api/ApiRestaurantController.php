<?php

namespace App\Http\Controllers\Api;


use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class ApiRestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNearestLocation(Request $request, $postCode)
    {
        // get locations and to array
        $restaurantsPostcodes = Restaurant::pluck('postcode');
        $post_data = array(
            'postcodes' => $restaurantsPostcodes
          );
          // send request with all postcodes
        $response = Http::withHeaders([
            'Content-Type' => 'application/json'
        ])->post('api.postcodes.io/postcodes', $post_data);

        //save data to data variable
        $data = $response->json();
        // save postcodes and long and lat into arrays
        $results = $data['result'];
            foreach($results as $result){
                
               $post = array('longitude' => $result['result']['latitude'] , 'latitude' => $result['result']['longitude']);
               $postcodes = array($result['result']['postcode'] => $post);
               var_dump($postcodes);
            }
            // function haversineGreatCircleDistance($postcodes)
            // {
            //     $earth_radius = 6371;
            //     $latitude1 = 59.807964;
            //     $longitude1 = -138.744045;
            //     foreach($postcodes as $postcode){
            //         $dLat = deg2rad($latitude2 - $latitude1);
            //         $dLon = deg2rad($longitude2 - $longitude1);
                
            //         $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
            //         $c = 2 * asin(sqrt($a));
            //         $d = $earth_radius * $c;
                
            //         return $d;
            //     }


            // }
        // return the address and opening times of the closes coffee drop
    }

}


