<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('locations', ['restaurants' => Restaurant::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $restaurant = new Restaurant();
        $restaurant->postcode = $request->postcode;
        $restaurant->open_Monday = $request->open_Monday;
        $restaurant->open_Tuesday = $request->open_Tuesday;
        $restaurant->open_Wednesday = $request->open_Wednesday;
        $restaurant->open_Thursday = $request->open_Thursday;
        $restaurant->open_Friday = $request->open_Friday;
        $restaurant->open_Saturday = $request->open_Saturday;
        $restaurant->open_Sunday = $request->open_Sunday;
        $restaurant->closed_Monday = $request->closed_Monday;
        $restaurant->closed_Tuesday = $request->closed_Tuesday;
        $restaurant->closed_Wednesday = $request->closed_Wednesday;
        $restaurant->closed_Thursday = $request->closed_Thursday;
        $restaurant->closed_Friday = $request->closed_Friday;
        $restaurant->closed_Saturday = $request->closed_Saturday;
        $restaurant->closed_Sunday = $request->closed_Sunday;
        $restaurant->save();

    }

}
