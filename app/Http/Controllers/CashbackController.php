<?php

namespace App\Http\Controllers;

use App\Models\Cashback;
use Illuminate\Http\Request;

class CashbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function calculateCashback(Request $request)
    {
// get data from request and set it to a num

$ristretto = (int)$request->ristretto;

$espresso = (int)$request->espresso;

$lungo = (int)$request->lungo;


// create function to calculate cashback for each cup
    function cashback($capAmount, $firstCBack, $secondCback, $thirdCback){
        $i = 1;
        $cashback = 0;
        while($i <= $capAmount && $i <= 50):
                 $cashback += $firstCBack;
                $i++;
        
        endwhile;
            while( $i <= $capAmount && (50 < $i && $i <= 500)){
                $cashback += $secondCback;
                $i++;
            }
            while( $i <= $capAmount && 500 < $i){
                $cashback += $thirdCback;
                $i++;
            }
            return $cashback;
        }
        //calculate cashbacks for each cup
        //calculate total cashback
        //format to british pound
        //save to database
        //dd the cashback amount
        
    $ristrettoCashback = cashback($ristretto, 2, 3, 5);
    $espressoCashback = cashback($espresso, 4, 6, 10);
    $lungoCashback = cashback($lungo, 6, 9, 15);
    $totalCashback = ($ristrettoCashback + $espressoCashback + $lungoCashback);
    $formattedCashback = '£' . number_format( $totalCashback, 2, '.', ',' );
    $cashback = new Cashback();
    $cashback->cashback_amount = $formattedCashback;
    $cashback->save();
    dd($formattedCashback);
   

    }

    

    
}
